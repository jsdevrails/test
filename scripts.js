function logIt(x) {
  console.log(x);
}

function add(a, b) {
  return a + b;
}

function subtract(a, b) {
  return a - b;
}

function multiply(a, b) {
  return a * b;
}

function divide(a, b) {
  return a / b;
}

function square(a) {
  return a * a;
}

function abs(a) {
  return Math.abs(a);
}

function pow(base, exp) {
  return Math.pow(base, exp);
}

function addOne(a) {
  return a + 1;
}
